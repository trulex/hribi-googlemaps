let bs = document.getElementsByTagName('b');
let array = Array.prototype.filter
	.call(bs, b => b.innerHTML === 'Izhodišče&nbsp;Širina/Dolžina:');

if (array.length) {
	let startingPointCoordinatesElement = array[0];
	let coordinatesElement = document.getElementById('kf0');
	let googleMapsCoordinates = coordinatesElement.innerHTML
		.replace(/,/g, '.').replace(/&nbsp;\/&nbsp;/g, ' ');
	let tbodyElement = coordinatesElement.closest("tbody");
	let tr = document.createElement('tr');
	let td = document.createElement('td');
	let b = document.createElement('b');
	let a = document.createElement('a');

	b.innerHTML = 'Google Maps: ';
	a.textContent = googleMapsCoordinates;
	a.setAttribute('href',
		'https://www.google.com/maps/search/?api=1&query=' + googleMapsCoordinates);
	a.setAttribute('target', '_blank');
	a.setAttribute('class', 'moder');

	td.appendChild(b);
	td.appendChild(a);
	tr.appendChild(td);
	tbodyElement.appendChild(tr);
}